package linkedlist;

public class Element<T> {
	private T item;
	private Element<T> next;
	private Element<T> prev;

    public Element(T item){
        this.item = item;
    }

	public T getItem() {
		return this.item;
	}

	public Element<T> getNext() {
		return this.next;
	}

	public void setNext(Element<T> element) {
		this.next = element;
	}

	public void setItem(T item) {
		this.item = item;
	}

	public void setPrevious(Element<T> element) {
		this.prev = element;
	}

	public Element<T> getPrevious() {
		return this.prev;
	}

	public boolean isEmpty(){
		return this.item==null;
	}

	public boolean isLastElem(){
		return !hasNext();
	}

    public boolean isFirstElem(){
		return !hasPrevious();
	}

    public boolean isSingleElem(){
		return isFirstElem() && isLastElem();
	}

    public boolean hasNext(){
		return this.next!=null;
	}

    public boolean hasPrevious(){
		return this.prev!=null;
	}
}