package linkedlist;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

public class LinkedList<T> implements List<T> {
    protected Element<T> element;

    public LinkedList(){

    }

    //listIteratorImpl
    public LinkedList(final Element<T> element) {
        this.element = element;
    }

    @Override
    public int size() {
        int size = 0;
        Element<T> next = this.element;
        while (next != null) {
            next = next.getNext();
            ++size;
        }
        return size;
    }

    @Override
    public boolean isEmpty() {
        boolean isEmpty = false;
        if (element == null) {
            isEmpty = true;
        }
        return isEmpty;
    }

    @Override
    public boolean contains(final Object item) {
        Element<T> elem = this.element;
        while (elem != null) {
            if (elem.getItem() == item) {
                return true;
            }
            elem = elem.getNext();
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            Element<T> elem = element;

            @Override
            public boolean hasNext() {
                return elem != null && !elem.isEmpty();
            }

            @Override
            public T next() {
                if (hasNext()) {
                    final Element<T> actual = elem;
                    elem = elem.getNext();
                    return actual.getItem();
                } else {
                    throw new NoSuchElementException();
                }
            }
        };
    }

    @Override
    public Object[] toArray() {
        Element<T> elem = this.element;
        int index = 0;
        final Object[] array = new Object[this.size()];
        while (elem != null) {
            array[index] = elem.getItem();
            ++index;
            elem = elem.getNext();
        }
        return array;
    }

    @Override
    public <T> T[] toArray(final T[] array) {
        for (int i = 0; i < array.length; ++i) {
            array[i] = (T) get(i);
        }
        return array;
    }

    @Override
    public boolean add(final T item) {
        if (this.isEmpty()) {
            this.element = new Element<>(item);
            return true;
        } else {
            Element<T> elem = this.element;
            while (elem != null) {
                if (elem.isLastElem()) {
                    final Element<T> newElem = new Element<>(item);
                    newElem.setPrevious(elem);
                    elem.setNext(newElem);
                    return true;
                }
                elem = elem.getNext();
            }
        }
        return false;
    }

    @Override
    public boolean remove(final Object item) {
        Element<T> elem = this.element;
        while (elem != null) {
            if (elem.getItem() == item) {
                if (size() == 1) {
                    clear();
                } else if (elem.isLastElem()) {
                    final Element<T> prev = elem.getPrevious();
                    prev.setNext(null);
                    this.element = prev;
                } else if (elem.isFirstElem()) {
                    final Element<T> next = elem.getNext();
                    next.setPrevious(null);
                    this.element = next;
                } else {
                    final Element<T> prev = elem.getPrevious();
                    final Element<T> next = elem.getNext();
                    next.setPrevious(prev);
                    prev.setNext(next);
                    this.element = prev;
                }
                return true;
            }
            elem = elem.getNext();
        }
        return false;
    }

    @Override
    public boolean containsAll(final Collection<?> collection) {
        boolean containsAll = !collection.isEmpty();
        for (final Object item : collection) {
            if (containsAll) {
                containsAll = this.contains(item);
            }
        }
        return containsAll;
    }

    @Override
    public boolean addAll(final Collection<? extends T> collection) {
        boolean addAll = !collection.isEmpty();
        for (final T item : collection) {
            if (addAll) {
                addAll = this.add(item);
            } else {
                this.add(item);
            }
        }
        return addAll;
    }

    @Override
    public boolean addAll(int index, final Collection<? extends T> collection) {
        boolean changed = !collection.isEmpty();
        for (final T item : collection) {
            add(index++, item);
            changed = true;
        }
        return changed;
    }

    @Override
    public boolean removeAll(final Collection<?> collection) {
        boolean changed = !collection.isEmpty();
        for (final Object item : collection) {
            remove(item);
            changed = true;
        }
        return changed;
    }

    @Override
    public boolean retainAll(final Collection<?> collection) {
        boolean changed = !collection.isEmpty();
        for (int i = 0; i < size(); ++i) {
            if (!collection.contains(get(i))) {
                remove(get(i));
                changed = true;
            }
        }
        return changed;
    }

    @Override
    public void clear() {
        this.element = null;
    }

    @Override
    public T get(final int index) {
        Element<T> elem = this.element;
        int searchIndex = 0;
        while (elem != null) {
            if (searchIndex == index) {
                return elem.getItem();
            }
            elem = elem.getNext();
            ++searchIndex;
        }
        return null;
    }

    @Override
    public T set(final int index, final T newItem) {
        Element<T> elem = this.element;
        int searchIndex = 0;
        while (elem != null) {
            if (searchIndex == index) {
                final T oldItem = elem.getItem();
                elem.setItem(newItem);
                return oldItem;
            }
            elem = elem.getNext();
            ++searchIndex;
        }
        return null;
    }

    @Override
    public void add(final int index, final T item) {
        if (this.isEmpty()) {
            this.element = new Element<>(item);
        } else {
            Element<T> elem = this.element;
            int searchIndex = 0;
            while (elem != null) {
                if (searchIndex == index) {
                    final Element<T> newElem = new Element<>(item);
                    final Element<T> prevElem = elem.getPrevious();
                    newElem.setPrevious(prevElem);
                    prevElem.setNext(newElem);

                    newElem.setNext(elem);
                    elem.setPrevious(newElem);
                    return;
                }
                elem = elem.getNext();
                ++searchIndex;
            }
        }
    }

    @Override
    public T remove(final int index) {
        final Element<T> elem = this.element;
        int searchIndex = 0;
        while (elem != null) {
            if (searchIndex == index) {
                this.remove(elem.getItem());
                return elem.getItem();
            }
            ++searchIndex;
        }
        return null;
    }

    @Override
    public int indexOf(final Object item) {
        int index = 0;
        Element<T> elem = this.element;
        while (elem != null) {
            if (elem.getItem() == item) {
                return index;
            }
            ++index;
            elem = elem.getNext();
        }
        return -1;
    }

    @Override
    public int lastIndexOf(final Object item) {
        int searchIndex = -1;
        for (int i = 0; i < size(); ++i) {
            if (get(i).equals(item)) {
                searchIndex = i;
            }
        }
        return searchIndex;
    }

    @Override
    public ListIterator<T> listIterator() {
        return listIteratorImpl(-1);
    }

    @Override
    public ListIterator<T> listIterator(final int index) {
        return listIteratorImpl(index);
    }

    private ListIterator<T> listIteratorImpl(final int indexParam) {
        return new ListIterator<T>() {
            private Integer index = -1;
            private Element<T> elem = syncElem();
            private final LinkedList<T> list = new LinkedList<>(element);

            private Element<T> syncElem() {
                this.elem = rootElem(element);
                while (index != indexParam) {
                    if (index < indexParam) {
                        next();
                    } else {
                        previous();
                    }
                }
                if (indexParam != -1) {
                    previous();
                }
                return this.elem;
            }

            private Element<T> rootElem(final Element<T> element) {
                final Element<T> root = new Element<>(null);
                root.setNext(element);
                return root;
            }

            @Override
            public boolean hasNext() {
                return this.elem.hasNext();
            }

            @Override
            public T next() {
                if (hasNext()) {
                    this.elem = this.elem.getNext();
                    ++index;
                    return this.elem.getItem();
                } else {
                    throw new NoSuchElementException();
                }
            }

            @Override
            public boolean hasPrevious() {
                return this.elem.hasPrevious();
            }

            @Override
            public T previous() {
                if (hasPrevious()) {
                    this.elem = this.elem.getPrevious();
                    --index;
                    return this.elem.getItem();
                }
                return null;
            }

            @Override
            public int nextIndex() {
                return this.index + 1;
            }

            @Override
            public int previousIndex() {
                return this.index - 1;
            }

            @Override
            public void remove() {

                if (list.size() == 1) {
                    list.clear();
                    this.elem = rootElem(null);
                } else if (this.elem.isLastElem()) {
                    list.remove(previous());
                    final Element<T> prev = this.elem.getPrevious();
                    prev.setNext(null);
                    this.elem = prev;
                } else if (this.elem.isFirstElem()) {
                    list.remove(this.elem.getItem());
                    final Element<T> next = this.elem.getNext();
                    next.setPrevious(null);
                    this.elem = this.rootElem(next);
                } else {
                    list.remove(previous());
                    final Element<T> prev = this.elem.getPrevious();
                    final Element<T> next = this.elem.getNext();
                    next.setPrevious(prev);
                    prev.setNext(next);
                    this.elem = prev;
                }

                element = list.element;
            }

            @Override
            public void set(final T item) {
                list.set(this.index, item);
            }

            @Override
            public void add(final T item) {
                list.add(item);
                if (this.elem.isLastElem() && this.elem.isEmpty()) {
                    this.elem = rootElem(list.element);
                }
            }
        };
    }

    @Override
    public List<T> subList(final int fromIndex, final int toIndex) {
        final List<T> subList = new LinkedList<>();
        for(int i=fromIndex; i<=toIndex; ++i){
            subList.add(get(i));
        }
        return subList;
    }
}