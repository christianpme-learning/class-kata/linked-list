package linkedlist;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class ElementTest{

    @Test
    public void elementInitTest(){
        Object item = new Object();
        Element<Object> element = new Element<>(item);

        Object actual = element.getItem();
        assertTrue(item.equals(actual));

        Element<Object> nextElem = element.getNext();
        assertTrue(null==nextElem);
    }

    @Test
    public void setNextTest(){
        Object item = new Object();
        Element<Object> element = new Element<>(item);

        Object nextItem = new Object();
        Element<Object> nextElem = new Element<Object>(nextItem);
        element.setNext(nextElem);

        Element<Object> actual = element.getNext();
        assertTrue(actual.getItem().equals(nextItem));
    }

    @Test
    public void setItemTest(){
        Object item = new Object();
        Element<Object> element = new Element<>(item);

        Object updateItem = new Object();
        element.setItem(updateItem);

        Object actual = element.getItem();
        assertTrue(actual.equals(updateItem));
    }

    @Test
    public void getItemTest(){
        Object item = new Object();
        Element<Object> element = new Element<>(item);

        assertTrue(item.equals(element.getItem()));
    }

    @Test
    public void setPreviousTest(){
        Object item = new Object();
        Element<Object> element = new Element<>(item);

        Object prevItem = new Object();
        Element<Object> prevElem = new Element<Object>(prevItem);
        element.setPrevious(prevElem);

        Element<Object> actual = element.getPrevious();
        assertTrue(actual.getItem().equals(prevItem));
    }

    @Test
    public void isEmptyTest(){
        Element<Object> elementEmpty = new Element<>(null);
        Element<Object> elementNotEmpty = new Element<>(new Object());
        assertTrue(elementEmpty.isEmpty());
        assertFalse(elementNotEmpty.isEmpty());
    }

    Element<Object> first;
    Element<Object> second;
    Element<Object> last;

    @Before
    public void initializeTests(){
        first = new Element<>(new Object());
        second = new Element<>(new Object());
        last = new Element<>(new Object());
        first.setNext(second);
        first.setPrevious(null);
        second.setNext(last);
        second.setPrevious(first);
        last.setNext(null);
        last.setPrevious(second);
    }

    @Test
    public void isLastElemTest(){
        assertTrue(last.isLastElem());
        assertFalse(first.isLastElem());
	}

    @Test
    public void isFirstElemTest(){
		assertTrue(first.isFirstElem());
        assertFalse(last.isFirstElem());
	}

    @Test
    public void isSingleElemTest(){
        Element<Object> singleElem = new Element<>(new Object());
        assertTrue(singleElem.isSingleElem());
        assertFalse(first.isSingleElem());
	}

    @Test
    public void hasNextTest(){
        assertTrue(first.hasNext());
        assertTrue(second.hasNext());
        assertFalse(last.hasNext());
    }
    
    @Test
    public void hasPrevious(){
		assertTrue(last.hasPrevious());
        assertTrue(second.hasPrevious());
        assertFalse(first.hasPrevious());
	}
}