package linkedlist;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.junit.Test;

public class LinkedListTest{

    @Test
    public void linkedListInitTest(){
        List<Object> list = new LinkedList<>();
        
        assertTrue(list.size()==0);
        assertTrue(list.isEmpty()==true);
    }

    @Test
    public void addItemTest(){
        List<Object> list = new LinkedList<>();

        Object obj = new Object();
        list.add(obj);

        Object actual = list.get(0);
        assertTrue(obj.equals(actual));
    }

    @Test
    public void addItemAtIndexTest(){
        List<Object> list = new LinkedList<>();

        list.add(new Object());
        list.add(new Object());

        Object obj = new Object();
        list.add(1, obj);

        assertTrue(obj.equals(list.get(1)));
    }

    @Test
    public void sizeTest(){
        List<Object> list = new LinkedList<>();

        list.add(new Object());
        list.add(new Object());

        int actual = list.size();

        assertTrue(actual==2);
    }

    @Test
    public void removeTest(){
        List<Object> list = new LinkedList<>();

        Object firstObj = new Object();
        Object secondObj = new Object();

        list.add(firstObj);
        list.add(secondObj);

        list.remove(firstObj);
        
        assertTrue(list.size()==1);
        assertTrue(list.get(0)==secondObj);
    }

    @Test
    public void removeAtIndexTest(){
        List<Object> list = new LinkedList<>();

        Object firstObj = new Object();
        Object secondObj = new Object();

        list.add(firstObj);
        list.add(secondObj);

        list.remove(0);

        assertTrue(list.get(0).equals(secondObj));
    }

    @Test
    public void clearTest(){
        List<Object> list = new LinkedList<>();

        list.add(new Object());
        list.add(new Object());
        list.add(new Object());

        list.clear();

        assertTrue(list.isEmpty());
        assertTrue(list.size()==0);
    }

    @Test
    public void containsTest(){
        List<Object> list = new LinkedList<>();

        Object firstObj = new Object();
        Object secondObj = new Object();

        list.add(firstObj);
        list.add(secondObj);

        assertTrue(list.contains(firstObj));
        assertTrue(list.contains(secondObj));
    }

    @Test
    public void indexOfTest(){
        List<Object> list = new LinkedList<>();

        Object obj = new Object();

        list.add(new Object());
        list.add(new Object());
        list.add(obj);

        assertTrue(2==list.indexOf(obj));
    }

    @Test
    public void setAtIndexTest(){
        List<Object> list = new LinkedList<>();

        list.add(new Object());
        list.add(new Object());
        list.add(new Object());

        Object obj = new Object();
        list.set(1, obj);

        assertTrue(list.get(1).equals(obj));
    }

    @Test
    public void getTest(){
        List<Object> list = new LinkedList<>();

        Object expected = new Object();
        list.add(new Object());
        list.add(expected);
        list.add(new Object());

        assertTrue(expected.equals(list.get(1)));
    }

    @Test
    public void toArrayTest(){
        Object[] expecteds = {new Object(), new Object(), new Object(), new Object(), new Object()};

        List<Object> list = new LinkedList<>();
        for(Object obj : expecteds){
            list.add(obj);
        }

        Object[] actuals = list.toArray();

        assertArrayEquals(expecteds, actuals);
    }

    @Test
    public void toArrayParamTest(){
        Object[] expecteds = {new Object(), new Object(), new Object(), new Object(), new Object()};

        List<Object> list = new LinkedList<>();
        for(Object obj : expecteds){
            list.add(obj);
        }
        
        assertArrayEquals(expecteds, list.toArray(new Object[list.size()]));
    }

    @Test
    public void iteratorTest(){
        List<Object> list = new LinkedList<>();
        Object first= new Object();
        list.add(first);
        Object second = new Object();
        list.add(second);
        Object last = new Object();
        list.add(last);

        Iterator<Object> iter = list.iterator();
        
        assertTrue(iter.next().equals(first));
        assertTrue(iter.hasNext());
        assertTrue(iter.next().equals(second));
        assertTrue(iter.hasNext());
        assertTrue(iter.next().equals(last));
        assertTrue(iter.hasNext()==false);
    }

    @Test
    public void listIteratorTest(){
        List<Object> list = new LinkedList<>();

        Object first= new Object();
        list.add(first);
        Object second = new Object();
        list.add(second);
        Object last = new Object();
        list.add(last);

        ListIterator<Object> iter = list.listIterator();

        assertTrue(iter.nextIndex()==0);
        assertTrue(iter.next().equals(first));
        assertTrue(iter.hasNext());

        assertTrue(iter.nextIndex()==1);
        assertTrue(iter.next().equals(second));
        assertTrue(iter.hasNext());

        assertTrue(iter.nextIndex()==2);
        assertTrue(iter.next().equals(last));
        assertTrue(iter.hasNext()==false);

        assertTrue(iter.hasPrevious());
        assertTrue(iter.previousIndex()==1);
        assertTrue(iter.previous().equals(second));

        assertTrue(iter.hasPrevious());
        assertTrue(iter.previousIndex()==0);
        assertTrue(iter.previous().equals(first));
        
        assertTrue(iter.hasPrevious()==false);
    }

    @Test
    public void listIteratorAddTest(){
        List<Object> list = new LinkedList<>();

        ListIterator<Object> iter = list.listIterator();

        Object first= new Object();
        iter.add(first);
        assertTrue(first.equals(iter.next()));

        Object second = new Object();
        iter.add(second);
        assertTrue(second.equals(iter.next()));

        Object last = new Object();
        iter.add(last);
        assertTrue(last.equals(iter.next()));
    }

    @Test
    public void listIteratorSetNextTest(){
        List<Object> list = new LinkedList<>();

        Object addFirst = new Object();
        list.add(addFirst);
        Object addSecond = new Object();
        list.add(addSecond);
        Object addLast = new Object();
        list.add(addLast);

        ListIterator<Object> iter = list.listIterator();

        assertTrue(addFirst.equals(iter.next()));
        Object setFirst = new Object();
        iter.set(setFirst);
        iter.next();
        assertTrue(setFirst.equals(iter.previous()));

        assertTrue(addSecond.equals(iter.next()));
        Object setSecond = new Object();
        iter.set(setSecond);
        iter.next();
        assertTrue(setSecond.equals(iter.previous()));

        assertTrue(addLast.equals(iter.next()));
        Object setLast = new Object();
        iter.set(setLast);
        iter.previous();
        assertTrue(setLast.equals(iter.next()));
    }

    @Test
    public void listIteratorSetPreviousTest(){
        List<Object> list = new LinkedList<>();

        Object addFirst = new Object();
        list.add(addFirst);
        Object addSecond = new Object();
        list.add(addSecond);
        Object addLast = new Object();
        list.add(addLast);

        ListIterator<Object> iter = list.listIterator();

        iter.next();
        iter.next();
        iter.next();

        assertTrue(addSecond.equals(iter.previous()));
        Object setSecond = new Object();
        iter.set(setSecond);
        iter.next();
        assertTrue(setSecond.equals(iter.previous()));

        assertTrue(addFirst.equals(iter.previous()));
        Object setFirst = new Object();
        iter.set(setFirst);
        iter.next();
        assertTrue(setFirst.equals(iter.previous()));
    }

    @Test
    public void listIteratorRemoveTest(){
        List<Object> list = new LinkedList<>();

        Object first= new Object();
        list.add(first);
        Object second = new Object();
        list.add(second);
        Object last = new Object();
        list.add(last);

        ListIterator<Object> iter = list.listIterator();

        assertTrue(first.equals(iter.next()));
        iter.remove();
        assertTrue(second.equals(iter.next()));
        iter.remove();
        assertTrue(last.equals(iter.next()));
        iter.remove();

        assertTrue(list.size()==0);
        assertTrue(list.isEmpty());
    }

    @Test
    public void listIteratorIndexParamTest(){
        List<Object> list = new LinkedList<>();

        Object first= new Object();
        list.add(first);
        Object second = new Object();
        list.add(second);
        Object last = new Object();
        list.add(last);

        ListIterator<Object> iter = list.listIterator(1);

        assertTrue(second.equals(iter.next()));
    }

    @Test
    public void subListTest(){
        List<Object> list = new LinkedList<>();

        Object first= new Object();
        list.add(first);
        Object second = new Object();
        list.add(second);
        Object last = new Object();
        list.add(last);

        List<Object> expected = new LinkedList<>();
        expected.add(second);
        expected.add(last);

        List<Object> actual = list.subList(1, 2);

        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void lastIndexOfTest(){
        List<Object> list = new LinkedList<>();

        Object first = new Object();
        Object duplicate = new Object();
        list.add(first);
        list.add(duplicate);
        list.add(duplicate);

        assertTrue(2==list.lastIndexOf(duplicate));
    }

    @Test
    public void containsAllTest(){
        List<Object> list = new LinkedList<>();
        list.add(new Object());
        list.add(new Object());

        Collection<Object> collection = new LinkedList<>();
        collection.add(new Object());
        collection.add(new Object());
        collection.add(new Object());
        collection.add(new Object());

        for(Object obj : collection){
            list.add(obj);
        }

        assertTrue(list.containsAll(collection));
    }

    @Test
    public void containsAllEmptyTest(){
        List<Object> list = new LinkedList<>();

        Collection<Object> collection = new LinkedList<>();

        assertFalse(list.containsAll(collection));
    }

    @Test
    public void addAllTest(){
        List<Object> actuals = new LinkedList<>();
        List<Object> expecteds = new LinkedList<>();
        
        Collection<Object> collection = new LinkedList<>();
        collection.add(new Object());
        collection.add(new Object());
        collection.add(new Object());
        collection.add(new Object());

        assertTrue(actuals.addAll(collection));

        for(Object obj : collection){
            expecteds.add(obj);
        }

        assertTrue(4==expecteds.size());
        assertArrayEquals(expecteds.toArray(), actuals.toArray());
    }

    @Test
    public void addAllEmptyTest(){
        List<Object> list = new LinkedList<>();

        Collection<Object> collection = new LinkedList<>();

        assertFalse(list.addAll(collection));
    }

    @Test
    public void addAllAtIndexTest(){
        List<Object> list = new LinkedList<>();
        list.add(new Object()); //0
        list.add(new Object()); //1
        //insert 
        list.add(new Object()); //2
        list.add(new Object()); //3

        List<Object> addList = new LinkedList<>();
        addList.add(new Object()); //2
        addList.add(new Object()); //3
        addList.add(new Object()); //4

        int index=1;
        assertTrue(list.addAll(index, addList));

        assertTrue(7==list.size());
        assertTrue(addList.get(0).equals(list.get(index)));
        assertTrue(addList.get(1).equals(list.get(index+1)));
        assertTrue(addList.get(2).equals(list.get(index+2)));
    }

    @Test
    public void removeAllTest(){
        List<Object> list = new LinkedList<>();
        Object first = new Object();
        Object second = new Object();
        Object last = new Object();
        Object remaining = new Object();

        list.add(first);
        list.add(remaining);
        list.add(second);
        list.add(last);
        
        Collection<Object> collection = new LinkedList<>();
        collection.add(first);
        collection.add(second);
        collection.add(last);

        assertTrue(list.removeAll(collection));
        assertTrue(list.size()==1);
        assertTrue(list.get(0).equals(remaining));
    }

    @Test
    public void retainAllTest(){
        List<Object> list = new LinkedList<>();
        Object first = new Object();
        Object second = new Object();
        Object last = new Object();
        Object removing = new Object();

        list.add(first);
        list.add(removing);
        list.add(second);
        list.add(last);
        
        Collection<Object> collection = new LinkedList<>();
        collection.add(first);
        collection.add(second);
        collection.add(last);

        assertTrue(list.retainAll(collection));
        assertTrue(list.size()==3);
        assertTrue(list.get(1).equals(second));
    }
}